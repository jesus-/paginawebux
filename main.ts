// Se define una contenedora para los periodos
let periodos: string[] = ["Periodo A","Periodo B"];
let actividades: string[] = ["Taller", "Laboratorio", "Prueba"]; 

// Se selecciona el objeto <select> de la página index.html para agregarle los
// elementos
let lstPeriodos: HTMLElement | null = document.querySelector("#lstPeriodos");
let lstActividades: HTMLElement | null = document.querySelector("#lstActividades"); 

//Se recorre la contenedora lstPeriodos y se agrega cada elemento al <select>
periodos.map((periodo) => {
    lstPeriodos.innerHTML += "<option value='" + periodo + "'>" + periodo + "</option>";
})

actividades.map((actividad) => {
    lstActividades.innerHTML += "<option value='" + actividad + "'>" + actividad + "</option>";
})



function darElemento(){
    //Se selecciona el objeto <select> de la página
    let lstPeriodos: HTMLSelectElement | null = document.querySelector("#lstPeriodos");
    //Se selecciona el objeto <label>
    let elemento: HTMLElement | null = document.querySelector("#lblInfo");
    let indice = lstPeriodos.selectedIndex;
    elemento.innerHTML = lstPeriodos.options[indice].value;
}


function darActividades(){
    //Se selecciona el objeto <select> de la página
    let lstActividades: HTMLSelectElement | null = document.querySelector("#lstActividades");
    //Se selecciona el objeto <label>
    let elemento: HTMLElement | null = document.querySelector("#lblInfo");
    let indice = lstActividades.selectedIndex;
    elemento.innerHTML = lstActividades.options[indice].value;
}


function actualizarPuntaje(){
    //Se obtiene el valor del objeto
    let lblPuntaje: HTMLElement | null = document.querySelector("#lblPuntaje");
    //Se obtiene el valor de los input para los resultados
    let lecturaCritica: HTMLInputElement | null = document.querySelector("#txtLC");
    let matematicas: HTMLInputElement | null = document.querySelector("#txtMat");
    let socialesCiudadanas: HTMLInputElement | null = document.querySelector("#txtSyC");
    let cienciasNaturales: HTMLInputElement | null = document.querySelector("#txtCN");
    let ingles: HTMLInputElement | null = document.querySelector("#txtIng");
    //Se calcula el puntaje total
    let puntajeTotal: number = Number(lecturaCritica.value) + Number(matematicas.value) +
    Number(socialesCiudadanas.value) + Number(cienciasNaturales.value) + Number(ingles.value);
    //Se publica el puntaje total
    lblPuntaje.innerHTML = "Puntaje total: " + String(puntajeTotal);
    }