// Se define una contenedora para los periodos
var periodos = ["Periodo A", "Periodo B"];
var actividades = ["Taller", "Laboratorio", "Prueba"];
// Se selecciona el objeto <select> de la página index.html para agregarle los
// elementos
var lstPeriodos = document.querySelector("#lstPeriodos");
var lstActividades = document.querySelector("#lstActividades");
//Se recorre la contenedora lstPeriodos y se agrega cada elemento al <select>
periodos.map(function (periodo) {
    lstPeriodos.innerHTML += "<option value='" + periodo + "'>" + periodo + "</option>";
});
actividades.map(function (actividad) {
    lstActividades.innerHTML += "<option value='" + actividad + "'>" + actividad + "</option>";
});
function darElemento() {
    //Se selecciona el objeto <select> de la página
    var lstPeriodos = document.querySelector("#lstPeriodos");
    //Se selecciona el objeto <label>
    var elemento = document.querySelector("#lblInfo");
    var indice = lstPeriodos.selectedIndex;
    elemento.innerHTML = lstPeriodos.options[indice].value;
}
function darActividades() {
    //Se selecciona el objeto <select> de la página
    var lstActividades = document.querySelector("#lstActividades");
    //Se selecciona el objeto <label>
    var elemento = document.querySelector("#lblInfo");
    var indice = lstActividades.selectedIndex;
    elemento.innerHTML = lstActividades.options[indice].value;
}
function actualizarPuntaje() {
    //Se obtiene el valor del objeto
    var lblPuntaje = document.querySelector("#lblPuntaje");
    //Se obtiene el valor de los input para los resultados
    var lecturaCritica = document.querySelector("#txtLC");
    var matematicas = document.querySelector("#txtMat");
    var socialesCiudadanas = document.querySelector("#txtSyC");
    var cienciasNaturales = document.querySelector("#txtCN");
    var ingles = document.querySelector("#txtIng");
    //Se calcula el puntaje total
    var puntajeTotal = Number(lecturaCritica.value) + Number(matematicas.value) +
        Number(socialesCiudadanas.value) + Number(cienciasNaturales.value) + Number(ingles.value);
    //Se publica el puntaje total
    lblPuntaje.innerHTML = "Puntaje total: " + String(puntajeTotal);
}
